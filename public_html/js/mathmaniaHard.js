/** Global var declaration **/

var count = 1;
var score = 0; 
var sign  = "+";
var isStarted = false;

/** Functions **/


function updateData() { //to update data after each answer
   var values = calc.add() ;
    $("#num1").html(values[0]);
    $("#num2").html(values[1]);
    $("#result").html(values[2]);
    $("#score") .html(score);
    clearInterval(myTimer);
}

function checkSum(num1 , sign , num2 , result , answer) {
   var answer = ( answer == "correct" ) ? true : false;
   if( ( eval(num1 + sign + num2) == result ) == answer ){
      correctAnswer();
   }else {
      gameOver();
   }
}

function myTimer(){
    myTimer = setInterval(function(){
        swal({
            title: "Your Time is Over",
            text: "You lost and your score is " + score,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Try again",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false },
             function(isConfirm){
            if (isConfirm) {
                         window.location.reload();
                } else { window.location.href='index.html' ;
        } }); }, 5000);
}




function correctAnswer() { //if result = answer --> count +1 and updateData for next question
   score += count * 1;
   updateData();
}

function wrongAnswer() { //if result != answer --> count reset and updateData for next question
   if (score > 0) {
      score = 0;

   }
   updateData();
}

function gameOver() {
    clearInterval(myTimer);
    swal({
            title: "Your Answer was Wrong",
            text: "You lost and your score is " + score,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Try Again",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false },
             function(isConfirm){
            if (isConfirm) {
                         window.location.reload();
                } else { window.setTimeout(function(){window.location.href = "index.html"},0);
        } });

}



function myStopFunction() {
    clearInterval(myTimer);
}

function againTimer(){
    myTimer = setInterval(function(){
        swal({
            title: "Your Time is Over",
            text: "You lost and your score is " + score,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Play again",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false },
             function(isConfirm){
            if (isConfirm) {
                         window.location.reload();
                } else { window.location.href='index.html' ;
        } }); }, 5000);
}

var calc = {
   add : function(){
      var rndNum1  = Math.floor((Math.random() * 20) + 1) ;
      var rndNum2  = Math.floor((Math.random() * 20) + 1) ;
      var result ;
      var t = new Date();
      if (t.getMilliseconds() % 2 == 0) {
         result = rndNum1  + rndNum2;
      }else{
         result =  Math.floor((Math.random() * 20) + 1) ;
      }
      return [rndNum1, rndNum2 , result];
   }
};


$(document).ready(function() {
    $("#progressBar").animate({width: '0%'}, 5000);
    $(".answer").on('click', function(){
        $("#progressBar").stop();
        $("#progressBar").css("width", "100%");
        $("#progressBar").animate({
          width: '0%'
        }, 5000);
        myStopFunction();
        againTimer();
        });
});

// Main function
function init(){
   $("#sign").html(sign);

   updateData();

   $(".answer").on('click', function(){
      checkSum(
         $("#num1").html(),
         $("#sign").html() ,
         $("#num2").html(),
         $("#result").html(),
         $(this).attr("id")
      );
   });
}

// Start the game ..
window.onload = init ;
